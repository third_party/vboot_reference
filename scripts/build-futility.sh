#!/bin/bash
#
# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -o errexit -o nounset

readonly SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly VBOOT_DIR="$(dirname "${SCRIPT_DIR}")"
readonly THIRD_PARTY_DIR="$(dirname "${VBOOT_DIR}")"

readonly CMAKE_PROGRAM=${CMAKE_PROGRAM:-cmake}
readonly NINJA_PROGRAM=${NINJA_PROGRAM:-ninja}

BORINGSSL_SRC="${THIRD_PARTY_DIR}/boringssl"
BORINGSSL_BUILD="${BORINGSSL_SRC}/build"
mkdir -p -- "${BORINGSSL_BUILD}"
pushd "${BORINGSSL_BUILD}"
[[ -f "${BORINGSSL_BUILD}/build.ninja" ]] || ${CMAKE_PROGRAM} -GNinja \
  -DCMAKE_MAKE_PROGRAM=${NINJA_PROGRAM} \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_C_FLAGS=-fPIC \
  ${CMAKE_EXTRA_ARGS:-} \
  ..
${NINJA_PROGRAM}
popd

LDFLAGS=
HAVE_MACOS=0
if [[ $(uname) == "Darwin" ]]; then
  HAVE_MACOS=1
else
  LDFLAGS="-lpthread"
fi

(
  cd "${VBOOT_DIR}"
  make LDFLAGS="${LDFLAGS}" HAVE_MACOS="${HAVE_MACOS}" CRYPTO_LIB="${BORINGSSL_BUILD}/crypto/libcrypto.a" OPENSSL_INC="${BORINGSSL_SRC}/include" ARCH=x86_64 `pwd`/build/futility/futility
)
